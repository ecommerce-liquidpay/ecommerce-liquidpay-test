
Oct 18,2021
Kiruthika Venkatesh

Scope :
1. To develop REST API to support ecommerce APP to search and create order testable through Swagger
2. To write Unit Test Case

Test Case Verified
1) the product must exist 
        - Tested for "Apple iPhone, Samsung Galaxy, ..."
2) the system should manage the products stock availability
        - Random Qty (10,5 ..)  assigned for each product and when not in stock error shown "Insufficient stock for Product xyz"
3) User must be able to create an order
        - verified by adding a Product
4) User must be able to search products filtered by product name and category
        - Tested for combination of Product name and Category - "iPhone | Electronic device", "Face wash | Health & Beauty" 

sql script
-----------
USE [eCommerceDB]
GO
SET IDENTITY_INSERT [dbo].[Products] ON 

INSERT [dbo].[Products] ([Id], [Name], [Rating], [Description], [Price]) VALUES (5, N'Samsung Galaxy M32', N'3.5', N'Samsung Galaxy M32 (8+128GB)', 338)
INSERT [dbo].[Products] ([Id], [Name], [Rating], [Description], [Price]) VALUES (6, N'Apple iPhone 13', N'4.2', N'Apple iPhone 13 128GB RED', 1299)
INSERT [dbo].[Products] ([Id], [Name], [Rating], [Description], [Price]) VALUES (7, N'Lenovo Bluetooth Headphones', N'3.7', N'Sweatproof Headset Wireless Running Headphone for Sports', 7.67)
INSERT [dbo].[Products] ([Id], [Name], [Rating], [Description], [Price]) VALUES (8, N'Xiaomi MI True wireless Earbuds', N'3.8', N'Xiaomi MI True wireless Earbuds Basic BT5.0 / Bluetooth 5.0 TWS Noise reduction Stereo bass Mi Earbuds AI Control', 11.9)
INSERT [dbo].[Products] ([Id], [Name], [Rating], [Description], [Price]) VALUES (9, N'JBL Bluetooth Earphone T280', N'4.1', N'T280TWS Bluetooth Earphone T280 TWS Sports Waterproof Wireless Earbuds Headphones With Mic Charging Case for JBL', 16.22)
INSERT [dbo].[Products] ([Id], [Name], [Rating], [Description], [Price]) VALUES (10, N'Samsung 27" Curved Monitor', N'3.9', N'Samsung 27" Curved Monitor C27F390F', 220)
INSERT [dbo].[Products] ([Id], [Name], [Rating], [Description], [Price]) VALUES (11, N'Lenovo ThinkVision Monitor', N'4.2', N'Lenovo ThinkVision T24i-10 USED 24 inch FHD 1920x1080 LED Backlit LCD Monitor HDMI', 125)
INSERT [dbo].[Products] ([Id], [Name], [Rating], [Description], [Price]) VALUES (12, N'Himalaya Purifying Neem Face Wash', N'3.2', N'[Buy 1 Free 1] 100ml Himalaya Purifying Neem Face Wash', 7.7)
INSERT [dbo].[Products] ([Id], [Name], [Rating], [Description], [Price]) VALUES (13, N'Casio Ladies Watch', N'3.6', N'[WatchSpree] Casio Ladies Standard Analog Two-Tone Stainless Steel Band Watch LTPV001SG-9B LTP-V001SG-9B', 25.2)
INSERT [dbo].[Products] ([Id], [Name], [Rating], [Description], [Price]) VALUES (14, N'Skechers WomensShoes', N'4.2', N'Skechers Womens Go Run 400 V2 Performance Womens Shoes - 128000-NVBL', 50)
SET IDENTITY_INSERT [dbo].[Products] OFF
GO
SET IDENTITY_INSERT [dbo].[Categories] ON 

INSERT [dbo].[Categories] ([Id], [ProductId], [Description]) VALUES (1, 5, N'Electronic Devices')
INSERT [dbo].[Categories] ([Id], [ProductId], [Description]) VALUES (2, 6, N'Electronic Devices')
INSERT [dbo].[Categories] ([Id], [ProductId], [Description]) VALUES (3, 7, N'Electronic Accessories')
INSERT [dbo].[Categories] ([Id], [ProductId], [Description]) VALUES (4, 8, N'Electronic Accessories')
INSERT [dbo].[Categories] ([Id], [ProductId], [Description]) VALUES (5, 9, N'Electronic Accessories')
INSERT [dbo].[Categories] ([Id], [ProductId], [Description]) VALUES (6, 10, N'Electronic Accessories')
INSERT [dbo].[Categories] ([Id], [ProductId], [Description]) VALUES (7, 11, N'Electronic Accessories')
INSERT [dbo].[Categories] ([Id], [ProductId], [Description]) VALUES (8, 12, N'Health & Beauty')
INSERT [dbo].[Categories] ([Id], [ProductId], [Description]) VALUES (9, 13, N'Watches')
Insert into Categories (ProductId,Description) values(10,14,,'Sports & Fitness')
SET IDENTITY_INSERT [dbo].[Categories] OFF
GO
SET IDENTITY_INSERT [dbo].[Inventories] ON 

INSERT [dbo].[Inventories] ([Id], [ProductId], [QuantityAvailable]) VALUES (1, 5, 10)
INSERT [dbo].[Inventories] ([Id], [ProductId], [QuantityAvailable]) VALUES (2, 6, 10)
INSERT [dbo].[Inventories] ([Id], [ProductId], [QuantityAvailable]) VALUES (3, 7, 30)
INSERT [dbo].[Inventories] ([Id], [ProductId], [QuantityAvailable]) VALUES (4, 8, 1)
INSERT [dbo].[Inventories] ([Id], [ProductId], [QuantityAvailable]) VALUES (5, 9, 0)
INSERT [dbo].[Inventories] ([Id], [ProductId], [QuantityAvailable]) VALUES (6, 10, 5)
INSERT [dbo].[Inventories] ([Id], [ProductId], [QuantityAvailable]) VALUES (7, 11, 2)
INSERT [dbo].[Inventories] ([Id], [ProductId], [QuantityAvailable]) VALUES (8, 12, 5)
INSERT [dbo].[Inventories] ([Id], [ProductId], [QuantityAvailable]) VALUES (9, 13, 5)
INSERT [dbo].[Inventories] ([Id], [ProductId], [QuantityAvailable]) VALUES (10, 14, 10)
SET IDENTITY_INSERT [dbo].[Inventories] OFF
GO




