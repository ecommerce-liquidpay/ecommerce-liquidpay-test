﻿using eCommerce_LiquidPay.Models.DomainModels;
using Microsoft.EntityFrameworkCore;
using System;

namespace eCommerce_LiquidPay
{
    public class eCommerceDbContext
     : DbContext
    {
        public eCommerceDbContext(DbContextOptions options)
            : base(options)
        {

        }

        public DbSet<Product> Products { get; set; }
        public DbSet<ProductCategory> Categories { get; set; }
        public DbSet<ProductInventory> Inventories { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetials> OrderDetails { get; set; }
    }
}
