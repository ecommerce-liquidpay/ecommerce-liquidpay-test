﻿using eCommerce_LiquidPay.Models.DomainModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eCommerce_LiquidPay.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private readonly eCommerceDbContext _dbContext;

        public ProductRepository(eCommerceDbContext dbContext)
        {
            _dbContext = dbContext;
        }
#pragma warning disable CS8632 // The annotation for nullable reference types should only be used in code within a '#nullable' annotations context.
        public Task<List<Product>> GetProducts(string? productName, string? category)
#pragma warning restore CS8632 // The annotation for nullable reference types should only be used in code within a '#nullable' annotations context.
        {
            var prod = _dbContext.Products.
               Include("Category");

            var filteredProducts = prod
                 .Where(x => x.Name.Contains(productName)).
                  Where(x=> x.Category.Description.Contains(category)).ToListAsync();

            return filteredProducts;
        }

        public async Task<Product> GetProductById(long prodId)
        {
            return await _dbContext.Products.
                    Include(x =>x.Category).
                    Include(x => x.Inventory)
                    .FirstOrDefaultAsync(x => x.Id == prodId);
        }

        public async Task UpdateProduct(Product prod)
        {
            _dbContext.Products.Update(prod);
            await _dbContext.SaveChangesAsync();

        }
    }
}
