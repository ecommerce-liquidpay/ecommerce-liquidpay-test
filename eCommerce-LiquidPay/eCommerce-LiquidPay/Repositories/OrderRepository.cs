﻿using eCommerce_LiquidPay.Dtos;
using eCommerce_LiquidPay.Models.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eCommerce_LiquidPay.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        private readonly eCommerceDbContext _dbContext;

        public OrderRepository(eCommerceDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task AddNewOrder(Order order)
        {
           await _dbContext.Orders.AddAsync(order);
           await _dbContext.SaveChangesAsync();
        }
    }
}
