﻿using eCommerce_LiquidPay.Dtos;
using eCommerce_LiquidPay.Models.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eCommerce_LiquidPay.Repositories
{
    public interface IOrderRepository
    {
        Task AddNewOrder(Order order);
    }
}
