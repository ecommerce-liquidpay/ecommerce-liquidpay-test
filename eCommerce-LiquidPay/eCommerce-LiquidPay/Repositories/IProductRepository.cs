﻿using eCommerce_LiquidPay.Models.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eCommerce_LiquidPay.Repositories
{
    public interface IProductRepository
    {
        Task<List<Product>> GetProducts(string? productName, string? category);

        Task<Product> GetProductById(long prodId);

        Task UpdateProduct(Product prod);

    }
}
