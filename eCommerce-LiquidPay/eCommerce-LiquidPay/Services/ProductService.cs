﻿using eCommerce_LiquidPay.Dtos;
using eCommerce_LiquidPay.Interfaces;
using eCommerce_LiquidPay.Models.DomainModels;
using eCommerce_LiquidPay.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eCommerce_LiquidPay.Services
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;
        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public async Task<List<ProductDto>> GetProducts(string productName, string category)
        {
            if(string.IsNullOrWhiteSpace(productName) && string.IsNullOrWhiteSpace(category))
            {
                throw new eCommerceException("Enter either a Product name or category");
            }
            else
            {
                if (productName == null)
                    productName = string.Empty;
                if (category == null)
                    category = string.Empty;
                var products = await _productRepository.GetProducts(productName, category);

                if (products.Count == 0)
                    throw new eCommerceException("Product does not exist");
                else
                    return Helper.MapProducts(products);

            }
           
        }
    }
}
