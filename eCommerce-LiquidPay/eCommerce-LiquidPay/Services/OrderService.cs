﻿using eCommerce_LiquidPay.Dtos;
using eCommerce_LiquidPay.Interfaces;
using eCommerce_LiquidPay.Models.DomainModels;
using eCommerce_LiquidPay.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eCommerce_LiquidPay.Services
{
    public class OrderService : IOrderService
    {
        private readonly eCommerceDbContext _dbContext;
        private readonly IOrderRepository _orderRepository;
        private readonly IProductRepository _productRepository;
        public OrderService(eCommerceDbContext dbContext, IOrderRepository orderRepository, IProductRepository productRepository)
        {
            _dbContext = dbContext;
            _orderRepository = orderRepository;
            _productRepository = productRepository;
        }
        public async Task AddNewOrder(OrderDtos orderDto)
        {
            using var transaction = _dbContext.Database.BeginTransaction();

            Order newOrder = new Order();
            Product prod = null;
            foreach (var x in orderDto.Products)
            {
                prod = await _productRepository.GetProductById(x.ProductId);
                if (prod != null)
                {
                    if (prod.Inventory.QuantityAvailable < x.Quantity)
                        throw new eCommerceException("Insufficent Stock available for " + x.Name);
                    else
                    {
                        newOrder.OrderDetails.Add(new OrderDetials
                        {
                            ProductId = x.ProductId,
                            QuantityOrdered = x.Quantity
                        });
                        newOrder.TotalPrice += x.Price * x.Quantity;

                        prod.Inventory.QuantityAvailable -= x.Quantity;
                    }
                }
            }
            newOrder.Status = Constants.OrderStatus.CompletedOrder;
            await _orderRepository.AddNewOrder(newOrder);
            await _productRepository.UpdateProduct(prod);

            transaction.Commit();

        }

    }
}
