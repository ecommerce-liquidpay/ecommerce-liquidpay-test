﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eCommerce_LiquidPay
{
    public class eCommerceException : Exception
    {
        public eCommerceException()
        {

        }
        public eCommerceException(string? message) : base (message)
        {

        }
    }
}
