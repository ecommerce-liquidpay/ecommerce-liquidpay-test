﻿using eCommerce_LiquidPay.Dtos;
using eCommerce_LiquidPay.Models.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eCommerce_LiquidPay.Interfaces
{
    public interface IProductService
    {
        Task<List<ProductDto>> GetProducts(string productName, string category);
    }
}
