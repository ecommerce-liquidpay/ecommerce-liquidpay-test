﻿using eCommerce_LiquidPay.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eCommerce_LiquidPay.Interfaces
{
    public interface IOrderService
    {
         Task AddNewOrder(OrderDtos orderDto);
    }
}
