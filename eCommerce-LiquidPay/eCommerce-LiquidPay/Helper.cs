﻿using eCommerce_LiquidPay.Dtos;
using eCommerce_LiquidPay.Models.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eCommerce_LiquidPay
{
    public static class Helper
    {
        public static List<ProductDto> MapProducts(List<Product> products)
        {
            List<ProductDto> productsList = new List<ProductDto>();

            foreach (var x in products)
            {
                productsList.Add(new ProductDto
                {  
                    ProductId = x.Id,
                    Name = x.Name,
                    Category = x.Category.Description,
                    Description = x.Description,
                    Price = x.Price,
                    Rating = x.Rating
                });
            }
            return productsList;

        }
    }
}
