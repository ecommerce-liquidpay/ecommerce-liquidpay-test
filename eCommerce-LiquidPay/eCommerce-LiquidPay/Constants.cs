﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eCommerce_LiquidPay
{
    public static class Constants
    {
        public static class OrderStatus
        {
            public const string OpenOrder = "Open";
            public const string CompletedOrder = "Completed";
        }       
    }
}
