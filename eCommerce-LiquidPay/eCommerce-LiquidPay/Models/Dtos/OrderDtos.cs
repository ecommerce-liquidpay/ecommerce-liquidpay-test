﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eCommerce_LiquidPay.Dtos
{
    public class OrderDtos
    {
        public List<CartItems> Products { get; set; }
        public decimal TotalPrice { get; set; }
        public string Status { get; set; }
    }
}
