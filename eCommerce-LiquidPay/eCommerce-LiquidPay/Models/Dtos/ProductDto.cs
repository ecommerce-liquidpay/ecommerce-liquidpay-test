﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eCommerce_LiquidPay.Dtos
{

    public class ProductDto
    {
        public long ProductId { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public string Rating { get; set; }
        public double Price { get; set; }
    }
    public class CartItems: ProductDto
    {
        public int Quantity { get; set; }
    }

}
