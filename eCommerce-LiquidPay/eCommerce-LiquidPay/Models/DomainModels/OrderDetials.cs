﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace eCommerce_LiquidPay.Models.DomainModels
{
    public class OrderDetials
    {
        [Key]
        public long Id { get; set; }
        [ForeignKey("Order")]
        public long OrderId { get; set; }
        [ForeignKey("Product")]
        public long ProductId { get; set; }
        public int QuantityOrdered { get; set; }

        public OrderDetials() { }

        public OrderDetials( long id, long orderId, long prdId, int qty)
        {
            Id = id;
            OrderId = orderId;
            ProductId = prdId;
            QuantityOrdered = qty;
        }
    }
}
