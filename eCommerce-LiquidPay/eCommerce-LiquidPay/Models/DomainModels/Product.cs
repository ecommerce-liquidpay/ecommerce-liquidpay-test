﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eCommerce_LiquidPay.Models.DomainModels
{
    public class Product
    {
        [Key]
        public long Id { get; set; }
        public string Name { get; set; }
        public string Rating { get; set; }
        public string Description { get; set; }
        public double Price { get;set; }
        public ProductInventory Inventory{ get; set; }
        public ProductCategory Category { get; set; }
    }

}
