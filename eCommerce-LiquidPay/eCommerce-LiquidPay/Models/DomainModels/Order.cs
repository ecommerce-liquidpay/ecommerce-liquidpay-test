﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eCommerce_LiquidPay.Models.DomainModels
{
    public class Order
    {
        [Key]
        public long Id { get; set; }
        public double TotalPrice { get; set; }
        public string Status { get; set; }
        public List<OrderDetials> OrderDetails { get; set; }

        public Order()
        {
            OrderDetails = new List<OrderDetials>();
            Status = Constants.OrderStatus.OpenOrder;
        }
    }
}
