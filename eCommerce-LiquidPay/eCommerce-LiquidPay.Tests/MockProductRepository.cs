﻿using eCommerce_LiquidPay.Models.DomainModels;
using eCommerce_LiquidPay.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCommerce_LiquidPay.Tests
{
    public class MockProductRepository : IProductRepository
    {
        private readonly List<Product> mockProducts;

        public MockProductRepository()
        {
            mockProducts = new List<Product> {
                new Product
            {
                Id =5,
                Name = "Samsung Galaxy M32",
                Description = "Samsung Galaxy M32 (8+128GB)",
                Category = new ProductCategory
                {
                    Id = 1,
                    ProductId = 5,
                    Description = "Electronic Devices"
                },
                Inventory = new ProductInventory
                {
                    ProductId = 5,
                    QuantityAvailable = 10,
                    Id = 1
                },
                Rating = "3.5",
                Price = 398
            },
                new Product
            {
                Id =6,
                Name = "Apple iPhone 13",
                Description = "Apple iPhone 13 128GB RED",
                Category = new ProductCategory
                {
                    Id = 2,
                    ProductId = 6,
                    Description = "Electronic Devices"
                },
                Inventory = new ProductInventory
                {
                    ProductId = 6,
                    QuantityAvailable = 5,
                    Id = 2
                },
                Rating = "4.5",
                Price = 1299
            },
                new Product
            {
                Id =7,
                Name = "JBL Bluetooth Earphone T280",
                Description = "T280TWS Bluetooth Earphone T280 TWS Sports Waterproof Wireless Earbuds Headphones With Mic Charging Case for JBL",
                Category = new ProductCategory
                {
                    Id = 3,
                    ProductId = 7,
                    Description = "Electronic Accessories"
                },
                Inventory = new ProductInventory
                {
                    ProductId = 7,
                    QuantityAvailable = 4,
                    Id = 3
                },
                Rating = "3.5",
                Price = 16.22
            },
                new Product
            {
                Id =14,
                Name = "Skechers WomensShoes",
                Description = "Skechers Womens Go Run 400 V2 Performance Womens Shoes - 128000-NVBL",
                Category = new ProductCategory
                {
                    Id =4,
                    ProductId = 14,
                    Description = "Sports & Fitness"
                },
                Inventory = new ProductInventory
                {
                    ProductId = 14,
                    QuantityAvailable = 10,
                    Id = 4
                },
                Rating = "3.5",
                Price = 50
            },
                new Product
            {
                Id =13,
                Name = "Casio Ladies Watch",
                Description = "[WatchSpree] Casio Ladies Standard Analog Two-Tone Stainless Steel Band Watch LTPV001SG-9B LTP-V001SG-9B",
                Category = new ProductCategory
                {
                    Id = 5,
                    ProductId = 13,
                    Description = "Watches"
                },
                Inventory = new ProductInventory
                {
                    ProductId = 13,
                    QuantityAvailable = 10,
                    Id = 5
                },
                Rating = "3.5",
                Price = 25.20
            }
            };
        }
        public Task<List<Product>> GetProducts(string productName, string category)
        {
            return Task.FromResult(mockProducts.Where(x => x.Name.Contains(productName, StringComparison.OrdinalIgnoreCase) && x.Category.Description.Contains(category, StringComparison.OrdinalIgnoreCase)).ToList());
        }

        public async Task<Product> GetProductById(long prodId)
        {
            return await Task.FromResult(mockProducts
                    .FirstOrDefault(x => x.Id == prodId));
        }

#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
        public async Task UpdateProduct(Product prod)
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
        {
            mockProducts.Remove(prod);
            mockProducts.Add(prod);
        }
    }
}
