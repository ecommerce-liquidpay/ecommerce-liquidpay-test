﻿using eCommerce_LiquidPay.Models.DomainModels;
using eCommerce_LiquidPay.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace eCommerce_LiquidPay.Tests
{
    public class MockOrderRepository : IOrderRepository
    {
        private readonly List<Order> mockOrders;
        public MockOrderRepository()
        {
            mockOrders = new List<Order> { new Order
            {
                Id = 1,
                OrderDetails = new List<OrderDetials>
                {
                    new OrderDetials
                    {
                        Id = 1,
                        OrderId = 1,
                        ProductId = 5,
                        QuantityOrdered = 1
                    }

                },
                TotalPrice = 398,
                Status = Constants.OrderStatus.CompletedOrder

            },
                new Order
                {
                    Id = 2,
                    OrderDetails = new List<OrderDetials>
                    {
                        new OrderDetials
                        {
                            Id = 2,
                            OrderId = 2,
                            ProductId = 5,
                            QuantityOrdered = 2
                        }

                    },
                    TotalPrice = 676,
                    Status = Constants.OrderStatus.CompletedOrder

                },
                new Order
                {
                    Id = 3,
                    OrderDetails = new List<OrderDetials>
                    {
                        new OrderDetials
                        {
                            Id = 3,
                            OrderId = 3,
                            ProductId = 6,
                            QuantityOrdered = 2
                        }

                    },
                    TotalPrice = 2598,
                    Status = Constants.OrderStatus.CompletedOrder

                },
                new Order
                {
                    Id = 4,
                    OrderDetails = new List<OrderDetials>
                    {
                        new OrderDetials
                        {
                            Id = 4,
                            OrderId = 4,
                            ProductId = 14,
                            QuantityOrdered = 2
                        }

                    },
                    TotalPrice = 100,
                    Status = Constants.OrderStatus.CompletedOrder

                },
                new Order
                {
                    Id = 5,
                    OrderDetails = new List<OrderDetials>
                    {
                        new OrderDetials
                        {
                            Id = 1,
                            OrderId = 5,
                            ProductId = 13,
                            QuantityOrdered = 2
                        }

                    },
                    TotalPrice = 50.40,
                    Status = Constants.OrderStatus.CompletedOrder

                }

            };
        }
#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
        public async Task AddNewOrder(Order order)
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
        {
            mockOrders.Add(order);
        }
    }
}
