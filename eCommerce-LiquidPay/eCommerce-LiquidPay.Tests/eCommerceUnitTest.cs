using eCommerce_LiquidPay.Dtos;
using eCommerce_LiquidPay.Interfaces;
using eCommerce_LiquidPay.Models.DomainModels;
using eCommerce_LiquidPay.Repositories;
using eCommerce_LiquidPay.Services;
using FakeItEasy;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace eCommerce_LiquidPay.Tests
{
    public class eCommerceUnitTest
    {
        private readonly MockProductRepository _mockProductRepository;
        private readonly MockOrderRepository _mockOrderRepository;
        private readonly IOrderService _orderService;
        private readonly IProductService _productService;
        private readonly eCommerceDbContext _dbContext;
        public eCommerceUnitTest()
        {
            var options = new DbContextOptionsBuilder<eCommerceDbContext>().UseInMemoryDatabase(Guid.NewGuid().ToString())
                .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning)).Options;
            _mockProductRepository = new MockProductRepository();
            _mockOrderRepository = new MockOrderRepository();
            _productService = new ProductService(_mockProductRepository);
            _dbContext = new eCommerceDbContext(options);
            _orderService = new OrderService(_dbContext, _mockOrderRepository,_mockProductRepository);

        }

        [Theory]
        [InlineData("Samsung Galaxy M32", "Electronic Devices", 1)]
        [InlineData("Samsung Galaxy M32", "", 2)]
        [InlineData("Samsung", "", 3)]
        [InlineData("SAMSUNG", "", 4)]
        [InlineData("SAMSUNG","ELECTRONIC DEVICES", 5)]
        [InlineData("SAMSUNG", "ELECTRONIC", 6)]
        [InlineData("SAMSUNG", "ELECTRONIC DEVICE", 7)]
        [InlineData("samsung", "electronic device", 8)]
        public async Task GetProduct_Success(string prodName, string prodCategory, int testCase)
        {
            //Act
            var products = await _productService.GetProducts(prodName, prodCategory);
            //Assert
            if (testCase == 1)
            {
                Assert.Equal("Samsung Galaxy M32", products.FirstOrDefault().Name);
                Assert.Equal("Electronic Devices", products.FirstOrDefault().Category);
                Assert.Equal("3.5", products.FirstOrDefault().Rating);
                Assert.Equal(398, products.FirstOrDefault().Price);
                Assert.Equal("Samsung Galaxy M32 (8+128GB)", products.FirstOrDefault().Description);
            }
            else if (testCase == 2)
            {
                Assert.Equal("Samsung Galaxy M32", products.FirstOrDefault().Name);
                Assert.Equal("3.5", products.FirstOrDefault().Rating);
                Assert.Equal(398, products.FirstOrDefault().Price);
                Assert.Equal("Samsung Galaxy M32 (8+128GB)", products.FirstOrDefault().Description);
            }
            else if(testCase == 3)
            {
                Assert.Equal("Samsung Galaxy M32", products.FirstOrDefault().Name);
                Assert.Equal("3.5", products.FirstOrDefault().Rating);
                Assert.Equal(398, products.FirstOrDefault().Price);
                Assert.Equal("Samsung Galaxy M32 (8+128GB)", products.FirstOrDefault().Description);
            }
            else if (testCase == 4)
            {
                Assert.Equal("Samsung Galaxy M32", products.FirstOrDefault().Name);
                Assert.Equal("3.5", products.FirstOrDefault().Rating);
                Assert.Equal(398, products.FirstOrDefault().Price);
                Assert.Equal("Samsung Galaxy M32 (8+128GB)", products.FirstOrDefault().Description);
            }
            else if (testCase == 5)
            {
                Assert.Equal("Samsung Galaxy M32", products.FirstOrDefault().Name);
                Assert.Equal("3.5", products.FirstOrDefault().Rating);
                Assert.Equal(398, products.FirstOrDefault().Price);
                Assert.Equal("Samsung Galaxy M32 (8+128GB)", products.FirstOrDefault().Description);

            }
            else if (testCase == 6)
            {
                Assert.Equal("Samsung Galaxy M32", products.FirstOrDefault().Name);
                Assert.Equal("3.5", products.FirstOrDefault().Rating);
                Assert.Equal(398, products.FirstOrDefault().Price);
                Assert.Equal("Samsung Galaxy M32 (8+128GB)", products.FirstOrDefault().Description);
            }
            else if (testCase == 7)
            {
                Assert.Equal("Samsung Galaxy M32", products.FirstOrDefault().Name);
                Assert.Equal("3.5", products.FirstOrDefault().Rating);
                Assert.Equal(398, products.FirstOrDefault().Price);
                Assert.Equal("Samsung Galaxy M32 (8+128GB)", products.FirstOrDefault().Description);
            }
            else if (testCase == 8)
            {
                Assert.Equal("Samsung Galaxy M32", products.FirstOrDefault().Name);
                Assert.Equal("3.5", products.FirstOrDefault().Rating);
                Assert.Equal(398, products.FirstOrDefault().Price);
                Assert.Equal("Samsung Galaxy M32 (8+128GB)", products.FirstOrDefault().Description);
            }


        }

        [Theory]
        [InlineData("", "Electronic Devices", 1)]
        [InlineData("", "Electronic", 2)]
        public async Task GetProduct_Success2(string prodName, string prodCategory, int testCase)
        {
            //Act
            var products = await _productService.GetProducts(prodName, prodCategory);
            //Assert
            if (testCase == 1)
            {
                Assert.Equal(2, products.Count);
            }
            else if (testCase == 2)
            {
                Assert.Equal(3, products.Count);
            }


        }

        [Theory]
        [InlineData("", "")]
        [InlineData("", "Electronics")]
        public void GetProduct_Fail(string prodName, string prodCategory)
        {
            //Act
            Assert.ThrowsAsync<eCommerceException>(() => _productService.GetProducts(prodName, prodCategory));

        }

        public static IEnumerable<object[]> GetOrders()
        {
            OrderDtos orderDtos1 = new OrderDtos
            {
                TotalPrice = 2598,
                Status = "Open",
                Products = new List<CartItems>
                {
                   new CartItems
                   {
                        Name = "Apple Iphone 13",
                        Category = "Electronic Devices",
                        Description ="Apple iPhone 13 128GB RED",
                        Price = 1299,
                        Quantity = 2,
                        Rating = "4.2",
                        ProductId = 6
                   }

                }
            };
            OrderDtos orderDtos2 = new OrderDtos
            {
                TotalPrice = 50.40m,
                Status = "Open",
                Products = new List<CartItems>
                {
                   new CartItems
                   {
                        Name = "Casio Ladies Watch",
                        Category = "Watches",
                        Description ="[WatchSpree] Casio Ladies Standard Analog Two-Tone Stainless Steel Band Watch LTPV001SG-9B LTP-V001SG-9B",
                        Price = 25.20,
                        Quantity = 2,
                        Rating = "3.5",
                        ProductId = 13
                   }

                }
            };

            yield return new object[]{ orderDtos1};
            yield return new object[] { orderDtos2};
        }



        public static IEnumerable<object[]> GetOrdersWithInsufficientStock()
        {
            OrderDtos orderDtos1 = new OrderDtos
            {
                TotalPrice = 97.32m,
                Status = "Open",
                Products = new List<CartItems>
                {
                   new CartItems
                   {
                        Name = "JBL Bluetooth Earphone T280",
                        Category = "Electronic Accessories",
                        Description ="T280TWS Bluetooth Earphone T280 TWS Sports Waterproof Wireless Earbuds Headphones With Mic Charging Case for JBL",
                        Price = 16.22,
                        Quantity = 6,
                        Rating = "3.5",
                        ProductId = 7
                   }

                }
            };

            yield return new object[] { orderDtos1 };
        }
        [Theory]
        [MemberData(nameof(GetOrders))]
        public async Task CreateOrder_Success(OrderDtos orderDtos)
        {
            foreach (var x in orderDtos.Products)
            {
                var beforeOrder = await _mockProductRepository.GetProductById(x.ProductId);
                //Act
                await _orderService.AddNewOrder(orderDtos);
                var afterOrder = await _mockProductRepository.GetProductById(x.ProductId);

                //Assert
                Assert.Equal(afterOrder.Inventory.QuantityAvailable, beforeOrder.Inventory.QuantityAvailable);
            }
        }

        [Theory]
        [MemberData(nameof(GetOrdersWithInsufficientStock))]
        public void CreateOrder_InsufficientStock(OrderDtos orderDtos)
        {
            //Act & Assert
            Assert.ThrowsAsync<eCommerceException>(() =>  _orderService.AddNewOrder(orderDtos));


        }

    }
}
